import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class PromptBox {

	public static void main(String[] args) throws InterruptedException {

		WebDriverManager.chromedriver().setup();
		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demo.seleniumeasy.com/javascript-alert-box-demo.html");
//		*****Simple alert box		
		driver.findElement(By.xpath("//button[text()='Click for Prompt Box']")).click();
		Alert alt = driver.switchTo().alert();
		Thread.sleep(4000);
		String str = alt.getText();
		System.out.println(str);
		alt.sendKeys("rounak");
		alt.accept();
		System.out.println(driver.findElement(By.id("prompt-demo")).getText());

	}
}
