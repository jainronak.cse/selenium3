import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class WindowHandle {

	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();
		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demo.seleniumeasy.com/window-popup-modal-demo.html");	
		String parentId = driver.getWindowHandle();
		driver.findElement(By.xpath("//a[text()='Follow All']")).click();
		Set<String> s = driver.getWindowHandles();	
		Iterator<String> itr = s.iterator();
		String cc = null ;
		while(itr.hasNext()) {
			String childWindow =itr.next();
			driver.switchTo().window(childWindow);
			String st = driver.getCurrentUrl();
			if(st.contains("demo.seleniumeasy") || st.contains("twitter")) {
				System.out.println(driver.getCurrentUrl());
				if(st.contains("twitter")) {
					 cc = childWindow;
				}
				
			}else {
				driver.close();
			}		
		}
		driver.switchTo().window(cc);
		System.out.println(driver.getCurrentUrl());
		driver.switchTo().window(parentId);
		System.out.println(driver.getCurrentUrl());
	}
	
	
	
}
