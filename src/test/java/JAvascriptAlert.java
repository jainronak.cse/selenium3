import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class JAvascriptAlert {

	public static void main(String[] args) throws InterruptedException {

		WebDriverManager.chromedriver().setup();
		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demo.seleniumeasy.com/javascript-alert-box-demo.html");
	
//		*****Simple alert box		
//		driver.findElement(By.xpath("//button[text()='Click me!']")).click();
//		Alert alt = driver.switchTo().alert();
//		Thread.sleep(4000);
//		String str = alt.getText();
//		System.out.println(str);
//		alt.accept();
		Alert alt;
		driver.findElement(By.xpath("(//button[text()='Click me!'])[2]")).click();
		alt = driver.switchTo().alert();
		Thread.sleep(4000);
		String str = alt.getText();
		System.out.println(str);
		alt.accept();
		String s = driver.findElement(By.id("confirm-demo")).getText();
		System.out.println(s);
		
		driver.findElement(By.xpath("(//button[text()='Click me!'])[2]")).click();
		alt = driver.switchTo().alert();
		Thread.sleep(4000);
		 str = alt.getText();
		System.out.println(str);
		alt.dismiss();
		 s = driver.findElement(By.id("confirm-demo")).getText();
		System.out.println(s);
		
	}
}
