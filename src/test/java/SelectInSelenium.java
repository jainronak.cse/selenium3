import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SelectInSelenium {

	public static void main(String[] args) throws InterruptedException {

		WebDriverManager.chromedriver().setup();
		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demo.seleniumeasy.com/basic-select-dropdown-demo.html");
		WebElement we = driver.findElement(By.id("select-demo"));
		
		Select sel = new Select(we);
//		sel.selectByIndex(1);
//		Thread.sleep(4000);
//		sel.selectByVisibleText("Wednesday");
//		Thread.sleep(4000);
//		sel.selectByValue("Saturday");		
		List<WebElement> lst = sel.getOptions();
		for(int i =0 ;i<lst.size();i++) {
			String str =lst.get(i).getText();
			System.out.println(str);
		}
		
		
		
		
		
		
		
		

	}
}
