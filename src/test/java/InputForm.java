import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class InputForm {

	public static void main(String[] args) throws InterruptedException {
		
		WebDriverManager.chromedriver().setup();	
		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demo.seleniumeasy.com/basic-first-form-demo.html");
		String str = "Ashima";
		WebElement we = driver.findElement(By.id("user-message"));
		we.sendKeys(str);
		driver.findElement(By.xpath("//button[text()='Show Message']")).click();
		String tex = driver.findElement(By.id("display")).getText();
		System.out.println(tex);
		Thread.sleep(4000);
		driver.quit();
	}
}
